package com.olukoye.hannah.journalapp;

/**
 * Created by hannaholukoye on 25/06/2018.
 */


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.olukoye.hannah.journalapp.database.DiaryEntry;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * This EntryAdapter creates and binds ViewHolders, that hold the description and priority of a task,
 * to a RecyclerView to efficiently display data.
 */
public class EntryAdapter extends RecyclerView.Adapter<EntryAdapter.TaskViewHolder> {

    // Constant for date format
    private static final String DATE_FORMAT = "dd/MM/yyy";

    // Member variable to handle item clicks
    final private ItemClickListener mItemClickListener;
    // Class variables for the List that holds task data and the Context
    private List<DiaryEntry> mDiaryEntries;
    private Context mContext;
    // Date formatter
    private SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

    /**
     * Constructor for the EntryAdapter that initializes the Context.
     *  @param context  the current Context
     * @param listener the ItemClickListener
     */
    public EntryAdapter(Context context, ViewDiary listener) {
        mContext = context;
        mItemClickListener = listener;
    }

    /**
     * Called when ViewHolders are created to fill a RecyclerView.
     *
     * @return A new TaskViewHolder that holds the view for each task
     */
    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate the diary_layout to a view
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.diary_layout, parent, false);

        return new TaskViewHolder(view);
    }

    /**
     * Called by the RecyclerView to display data at a specified position in the Cursor.
     *
     * @param holder   The ViewHolder to bind Cursor data to
     * @param position The position of the data in the Cursor
     */
    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        // Determine the values of the wanted data
        DiaryEntry diaryEntry = mDiaryEntries.get(position);
        String title = diaryEntry.getTitle();
        String description = diaryEntry.getDescription();
        String updatedAt = dateFormat.format(diaryEntry.getUpdatedAt());

        //Set values
        holder.entryTitleView.setText(title);
        holder.entryDescriptionView.setText(description);
        holder.updatedAtView.setText(updatedAt);

    }

    @Override
    public int getItemCount() {
        if (mDiaryEntries == null) {
            return 0;
        }
        return mDiaryEntries.size();
    }

    public List<DiaryEntry> getTasks() {
        return mDiaryEntries;
    }

    public void setTasks(List<DiaryEntry> taskEntries) {
        mDiaryEntries = taskEntries;
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClickListener(int itemId);
    }

    class TaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView entryTitleView;
        TextView entryDescriptionView;
        TextView updatedAtView;


        public TaskViewHolder(View itemView) {
            super(itemView);
            entryTitleView = itemView.findViewById(R.id.entryTitle);
            entryDescriptionView = itemView.findViewById(R.id.entryDescription);
            updatedAtView = itemView.findViewById(R.id.entryUpdatedAt);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int elementId = mDiaryEntries.get(getAdapterPosition()).getId();
            mItemClickListener.onItemClickListener(elementId);
        }
    }
}
