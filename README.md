# Journal App Android App
This is a simple app provided to store/edit diary entries from a user and allows them to view their entries.

## What you will find:

- Register and Login using google authentication.
- View all entries.
- View the contents of a diary entry.
- Add and modify an entry.

### How to download the APK

1. Go to the build folder.
2. Select the outputs folder.
3. Select the apk folder then the debug folder and click on the .apk file to download.

## Android Version Targeting

Currently built to work with Android API 27. **However**, the minimum SDK support is API Version 15.

Please feel free to submit issues with any bugs or other unforseen issues you experience. 