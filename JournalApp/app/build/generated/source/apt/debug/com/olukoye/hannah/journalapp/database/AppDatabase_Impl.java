package com.olukoye.hannah.journalapp.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.util.HashMap;
import java.util.HashSet;

public class AppDatabase_Impl extends AppDatabase {
  private volatile DiaryDao _diaryDao;

  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `DiaryEntry` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT, `description` TEXT, `updated_at` INTEGER)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"1bbbe3e598c6353c25ea177473ca711c\")");
      }

      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `DiaryEntry`");
      }

      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsDiaryEntry = new HashMap<String, TableInfo.Column>(4);
        _columnsDiaryEntry.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsDiaryEntry.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsDiaryEntry.put("description", new TableInfo.Column("description", "TEXT", false, 0));
        _columnsDiaryEntry.put("updated_at", new TableInfo.Column("updated_at", "INTEGER", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysDiaryEntry = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesDiaryEntry = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoDiaryEntry = new TableInfo("DiaryEntry", _columnsDiaryEntry, _foreignKeysDiaryEntry, _indicesDiaryEntry);
        final TableInfo _existingDiaryEntry = TableInfo.read(_db, "DiaryEntry");
        if (! _infoDiaryEntry.equals(_existingDiaryEntry)) {
          throw new IllegalStateException("Migration didn't properly handle DiaryEntry(com.olukoye.hannah.journalapp.database.DiaryEntry).\n"
                  + " Expected:\n" + _infoDiaryEntry + "\n"
                  + " Found:\n" + _existingDiaryEntry);
        }
      }
    }, "1bbbe3e598c6353c25ea177473ca711c");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "DiaryEntry");
  }

  @Override
  public DiaryDao taskDao() {
    if (_diaryDao != null) {
      return _diaryDao;
    } else {
      synchronized(this) {
        if(_diaryDao == null) {
          _diaryDao = new DiaryDao_Impl(this);
        }
        return _diaryDao;
      }
    }
  }
}
