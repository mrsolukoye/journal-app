package com.olukoye.hannah.journalapp;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.olukoye.hannah.journalapp.database.AppDatabase;
import com.olukoye.hannah.journalapp.database.DiaryEntry;

import java.util.Date;

public class AddEntryActivity extends AppCompatActivity {
    public static final String EXTRA_TASK_ID = "extraTaskId";
    public static final String INSTANCE_TASK_ID = "instanceTaskId";

    // Constant for default task id to be used when not in update mode
    private static final int DEFAULT_ENTRY_ID = -1;
    // Constant for logging
    private static final String TAG = AddEntryActivity.class.getSimpleName();
    // Fields for views
    EditText mEditTextTitle, mEditTextDescription;
    Button mButton;

    private int mTaskId = DEFAULT_ENTRY_ID;

    // Member variable for the Database
    private AppDatabase mDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_entry);

        initViews();

        mDb = AppDatabase.getInstance(getApplicationContext());

        if (savedInstanceState != null && savedInstanceState.containsKey(INSTANCE_TASK_ID)) {
            mTaskId = savedInstanceState.getInt(INSTANCE_TASK_ID, DEFAULT_ENTRY_ID);
        }

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(EXTRA_TASK_ID)) {
            mButton.setText(R.string.update_button);
            if (mTaskId == DEFAULT_ENTRY_ID) {
                // populate the UI
                mTaskId = intent.getIntExtra(EXTRA_TASK_ID, DEFAULT_ENTRY_ID);

                Log.d(TAG, "Actively retrieving a specific task from the DataBase");
                final LiveData<DiaryEntry> task = mDb.taskDao().loadEntryById(mTaskId);
                task.observe(this, new Observer<DiaryEntry>() {
                    @Override
                    public void onChanged(@Nullable DiaryEntry diaryEntry) {
                        // COMPLETED (5) Remove the observer as we do not need it any more
                        task.removeObserver(this);
                        Log.d(TAG, "Receiving database update from LiveData");
                        populateUI(diaryEntry);
                    }
                });
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(INSTANCE_TASK_ID, mTaskId);
        super.onSaveInstanceState(outState);
    }

    /**
     * initViews is called from onCreate to init the member variable views
     */
    private void initViews() {
        mEditTextTitle = findViewById(R.id.editTextEntryTitle);
        mEditTextDescription = findViewById(R.id.editTextEntryDescription);
        mButton = findViewById(R.id.saveButton);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSaveButtonClicked();
            }
        });
    }

    /**
     * populateUI would be called to populate the UI when in update mode
     *
     * @param task the taskEntry to populate the UI
     */
    private void populateUI(DiaryEntry task) {
        if (task == null) {
            return;
        }
        mEditTextTitle.setText(task.getTitle());
        mEditTextDescription.setText(task.getDescription());
    }

    /**
     * onSaveButtonClicked is called when the "save" button is clicked.
     * It retrieves user input and inserts that new task data into the underlying database.
     */
    public void onSaveButtonClicked() {
        String title = mEditTextTitle.getText().toString();
        String description = mEditTextDescription.getText().toString();
        Date date = new Date();

        final DiaryEntry task = new DiaryEntry(title, description, date);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                if (mTaskId == DEFAULT_ENTRY_ID) {
                    // insert new task
                    mDb.taskDao().insertEntry(task);
                } else {
                    //update task
                    task.setId(mTaskId);
                    mDb.taskDao().updateEntry(task);
                }
                finish();
            }
        });
    }

}
